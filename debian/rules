#!/usr/bin/make -f
%:
	dh $@

execute_after_dh_auto_build:
	# Parts from the symfony package’s debian/rules.
	mkdir --parents debian/packages_to_build debian/autoloaders
	# Walk through the parts of upstream's code that should be packaged into
	# separate Debian binary packages and write down a package-to-build info
	# file containing shell variables for each package.
	# Those files will be traverse later on in different targets of this
	# makefile (debian/rules). They solely exists to not repeat the extraction
	# of those variables in each of those target over and over again.
	# Then, build a class loader for the package, using the template in
	# debian/$deb_pkg_name.autoload.php.tpl if it exists (to load dependencies).
	set -e;\
	 for src_path in $$(find src -mindepth 1 -maxdepth 2 -type d); do \
	  if [ -e $$src_path/composer.json ]; then \
	   pkg_path=$${src_path#src/}; \
	   deb_pkg_name=php-$$(cat $$src_path/composer.json | jq -r '.name | tostring' | sed -r 's|/|-|'); \
	   if [ $$(grep -c -E -e "^Package: $$deb_pkg_name\$$" debian/control) -ne 1 ]; then \
	    echo "W: No Debian package '$$deb_pkg_name' defined in debian/control," 1>&2; \
	    echo "   therefor not considering Symfony $$pkg_path," 1>&2; \
	    continue; \
	   fi; \
	   echo "# This file contains some variables sourced" > debian/packages_to_build/$$deb_pkg_name; \
	   echo "# by various targets in debian/rules" >> debian/packages_to_build/$$deb_pkg_name; \
	   echo "deb_pkg_name='$$deb_pkg_name'" >> debian/packages_to_build/$$deb_pkg_name; \
	   echo "src_path='$$src_path'" >> debian/packages_to_build/$$deb_pkg_name; \
	   echo "pkg_path='$$pkg_path'" >> debian/packages_to_build/$$deb_pkg_name; \
	   echo "symfony $${deb_pkg_name#php-symfony-} Symfony/Polyfill/$$pkg_path/autoload.php" > debian/autoloaders/$$deb_pkg_name; \
	   if [ -e debian/$$deb_pkg_name.autoload.php ]; then \
	    cp debian/$$deb_pkg_name.autoload.php \
		$$src_path/autoload.php ;\
	   else \
	    phpabtpl --basedir $$src_path $$src_path/composer.json > debian/$$deb_pkg_name.autoload.php.tpl; \
	    phpab \
		--exclude '*bootstrap*' \
		--output $$src_path/autoload.php \
		--tolerant \
		--template debian/$$deb_pkg_name.autoload.php.tpl \
		$$src_path; \
	   fi; \
	  fi; \
	 done
	# Mimic expected path for tests
	mkdir --parent Symfony vendor
	ln -s ../src Symfony/Polyfill
	cp debian/autoload.php src
	cp debian/autoload-tests.php vendor/autoload.php

override_dh_auto_test:
	phpunit -v

override_dh_install:
	dh_install --package=php-symfony-polyfill
	# In debian/packages_to_build/ a file containing shell variables exists
	# for each package that should be build. Source one file after another
	# to make the shell variables available and run dh_install in order to
	# provide the PHP runtime code for each package.
	set -e; \
	 for package_info_file in $$(find debian/packages_to_build/ -mindepth 1 -maxdepth 1 -type f); do \
	  . $$package_info_file; \
	  dh_install --package=$$deb_pkg_name \
		debian/autoloaders/$$deb_pkg_name usr/share/pkg-php-tools/autoloaders; \
	  dh_install \
		-Xcomposer.json \
		-XLICENSE \
		-XREADME.md \
		--package=$$deb_pkg_name \
		$$src_path/* \
		usr/share/php/Symfony/Polyfill/$$pkg_path/; \
	 done

override_dh_phpcomposer:
	dh_phpcomposer --package=php-symfony-polyfill
	# In debian/packages_to_build/ a file containing shell variables exists
	# for each package that should be build. Source one file after another
	# to make the shell variables available and run dh_phpcomposer for each
	# package.
	set -e;\
	 for package_info_file in $$(find debian/packages_to_build/ -mindepth 1 -maxdepth 1 -type f); do \
	  . $$package_info_file; \
	  dh_phpcomposer --package=$$deb_pkg_name --sourcedirectory=$$src_path; \
	 done
