<?php

require_once 'Symfony/Polyfill/Apcu/autoload.php';
require_once 'Symfony/Polyfill/Ctype/autoload.php';
require_once 'Symfony/Polyfill/Iconv/autoload.php';
require_once 'Symfony/Polyfill/Intl/Grapheme/autoload.php';
require_once 'Symfony/Polyfill/Intl/Icu/autoload.php';
require_once 'Symfony/Polyfill/Intl/Idn/autoload.php';
require_once 'Symfony/Polyfill/Intl/MessageFormatter/autoload.php';
require_once 'Symfony/Polyfill/Intl/Normalizer/autoload.php';
require_once 'Symfony/Polyfill/Mbstring/autoload.php';
// require_once 'Symfony/Polyfill/Php72/autoload.php'; (already required by Intl/Idn/autoload.php)
require_once 'Symfony/Polyfill/Php73/autoload.php';
require_once 'Symfony/Polyfill/Php74/autoload.php';
require_once 'Symfony/Polyfill/Php80/autoload.php';
require_once 'Symfony/Polyfill/Php81/autoload.php';
require_once 'Symfony/Polyfill/Util/autoload.php';
require_once 'Symfony/Polyfill/Uuid/autoload.php';

require_once __DIR__ . '/bootstrap.php';
